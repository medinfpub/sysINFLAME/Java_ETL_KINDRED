package sysINFLAME;

import com.talend.csv.CSVReader;
import com.talend.csv.CSVWriter;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import routines.TalendDate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Baum_Benjamin on 26.04.2017.
 */
public class KP_EXP9_Phenotypes {
    private static Logger log = Logger.getLogger(KP_EXP9_Phenotypes.class.getName());

    public void generateTranSMARTConfig(File dataTable,File dataTableMapped,File codebook,File mappingFile){

        try {
            HashMap<String, KP_EXP9_Phenotypes_Item> columnNameMap = new HashMap<>();
            HashMap<Integer,String> columnNameMapByPos = new HashMap<>();


            CSVReader dataReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
            CSVReader codebookReader = new CSVReader(codebook.getAbsolutePath(), ',', "UTF-8");
            CSVWriter writer = new CSVWriter(new FileWriter(mappingFile));
            writer.setSeparator('\t');

            CSVWriter dataMappedWriter = new CSVWriter(new FileWriter(dataTableMapped));
            dataMappedWriter.setSeparator('\t');

            FileInputStream file = new FileInputStream(codebook);

//Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook (file);

//Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(1);

//Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = sheet.iterator();

//Get iterator to all cells of current row
//            Iterator<Cell> cellIterator = row.cellIterator();

            //Skip first row
            rowIterator.next();

            while(rowIterator.hasNext()) {
                Row row = rowIterator.next();

                String trait = row.getCell(0).getStringCellValue();


                String trait_id = row.getCell(1).getStringCellValue();
                String trait_name = row.getCell(2).getStringCellValue();
                String description = row.getCell(3).getStringCellValue();
                String value = row.getCell(4).getStringCellValue();
                String value_description = row.getCell(5).getStringCellValue();
                String source_token = row.getCell(6).getStringCellValue();

                if (columnNameMap.containsKey(trait)) {
                    KP_EXP9_Phenotypes_Item item = columnNameMap.get(trait);
                    if (item.getType()!="NUMERICAL")
                        item.add(value,value_description);



                }
                else {
                    KP_EXP9_Phenotypes_Item item = new KP_EXP9_Phenotypes_Item(trait, trait_id, trait_name, description, value, value_description,source_token);
                    if (value_description.equalsIgnoreCase("continuous"))
                        item.setType("NUMERICAL");
                    else
                        item.setType("CATEGORICAL");
                    columnNameMap.put(trait, item);

                }

            }
            log.debug(columnNameMap.toString());
            mappingFile.delete();
            int lineCounter = 0;
//            while (codebookReader.readNext()) {
//                String [] input = codebookReader.getValues();
//                log.debug(input.length);
//                log.debug(input[0]);
            //


//            }

//             Write the List to a Mapping CSV File.
//            writer.writeAll(outputList);
//            writer.close();
//            log.debug(columnNameMap);


            /**
             *  Write transmart-batch config
             */
            // Header
            String [] output = new String[7];
            // Config Structure
            // Filename        Category_Code   Column_Number   Data_Label      Data_Label_Source       Control_Vocab_Cd        Concept_Type
            output[0] = "Filename";
            output[1] = "Category_Code";
            output[2] = "Column_Number";
            output[3] = "Data_Label";
            output[4] = "Data_Label_Source";
            output[5] = "Control_Vocab_Cd";
            output[6] = "Concept_Type";
            writer.writeNext(output);

            dataReader.readHeaders();
            int counter = 1;
            for (String header : dataReader.getHeaders()) {
                header = header.equalsIgnoreCase("new_id")?"new_id":header.substring(0,header.lastIndexOf("_"));
                output[0] = "phenotypes_MAPPED.csv";
                output[1] = "Popgen Phenotypes";// + " + header;
                output[2] = "" + counter;
                output[3] = header.equalsIgnoreCase("new_id") ? "SUBJ_ID" : columnNameMap.containsKey(header) ?
                        columnNameMap.get(header).getDescription() + "_"+columnNameMap.get(header).getSource_token()+"_"+columnNameMap.get(header).getTrait_id() : header;
                output[4] = "";
                output[5] = "";
                output[6] = header.equalsIgnoreCase("new_id")?"CATEGORICAL":columnNameMap.get(header).getType();
                writer.writeNext(output);
                counter++;
            }

            // Let's compute the AGE of the patients
            output[0] = "phenotypes_MAPPED.csv";
            output[1] = "Popgen Phenotypes";// + " + header;
            output[2] = "" + counter;
            output[3] = "AGE";
            output[4] = "";
            output[5] = "";
            output[6] = "NUMERICAL";
            writer.writeNext(output);
            writer.close();





            /**
             * Data cleaning
             */

            dataReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
            dataReader.readHeaders();
            String[] headers = dataReader.getHeaders();
            int i = 0;
            for (String s : headers){
                columnNameMapByPos.put(i++,s);
            }
            // Add AGE header column
            int outputSize = headers.length;
            String[] header = dataReader.getHeaders();
            String[] newHeader = new String[header.length+1];
            for (int k = 0; k < header.length; k++){
                newHeader[k] = header[k];
            }
            newHeader[header.length] = "AGE";

            dataMappedWriter.writeNext(newHeader);

            dataReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
            dataReader.readNext();


            // Read through the actual data and replace "NA", "77777777777" and "9999999999"
            while (dataReader.readNext()) {
                String[] outputString  = new String[outputSize+1];
                String [] input = dataReader.getValues();
                long age = -1;
                for (int j = 0; j < input.length; j++){
                    if (j > 0) {
                        String trait = columnNameMapByPos.get(j).substring(0,columnNameMapByPos.get(j).lastIndexOf("_"));

                        if (input[j].equalsIgnoreCase("na") || input[j].equalsIgnoreCase("9999999")
                                || input[j].equalsIgnoreCase("7777777")){
                            outputString[j] = "";
                        }
                        else if (columnNameMap.get(trait).getValueMap().get(input[j]) != null)
                            outputString[j] = columnNameMap.get(trait).getValueMap().get(input[j]);
                        // Sometimes, there are ">" and "<" in the data, remove them
                        else if (input[j].contains(">") || input[j].contains("<")  )
                            outputString[j] = input[j].replaceAll(">","").replaceAll("<","");
                        else
                            outputString[j] = input[j];

                        if (trait.toLowerCase().contains("year_of_birth")){
                            if (outputString[j].length()==4) {
                                Date date = TalendDate.parseDate("yyyy", outputString[j]);
                                Date current = TalendDate.getCurrentDate();
                                age = TalendDate.diffDate(current, date, "yyyy");
                            }
                        }
                    }
                    else {
                            outputString[j] = input[j];
                    }
                }
                if (age < 0)
                    outputString[outputSize] = "";
                else
                    outputString[outputSize] = "" + age;
                dataMappedWriter.writeNext(outputString);
            }
            dataMappedWriter.close();
            log.debug(columnNameMap.get("t13578_Calprotectin"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

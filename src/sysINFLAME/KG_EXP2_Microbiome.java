package sysINFLAME;

import com.talend.csv.CSVReader;
import com.talend.csv.CSVWriter;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Baum_Benjamin on 30.03.2017.
 */

/**
 * Splits the Big Microbiome datatable in smaller files (see @_LIMIT)
 */
public class KG_EXP2_Microbiome {
    static Logger log = Logger.getLogger(KG_EXP2_Microbiome.class.getName());
    static CSVReader dataTableReader;
    static CSVWriter writer;
    static CSVReader sintaxReader;
    static CSVReader utaxReader;
    static int _LIMIT = 100;

    public static void generateTranSMARTConfig(File dataTable,File sintax) {
        try {

            THashMap<String, String> sintaxMap = new THashMap<>();


            File batchConfig = new File("C:\\Desktop_Workspace\\sysINFLAME\\KG_EXP2_Microbiome_done\\clinical_mapping_file.csv");

            dataTableReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
            sintaxReader = new CSVReader(sintax.getAbsolutePath(), '\t', "UTF-8");
            writer = new CSVWriter(new FileWriter(batchConfig));
            writer.setSeparator('\t');


            while (sintaxReader.readNext()) { //
                String[] output = sintaxReader.getValues();
                sintaxMap.put(output[0],output[17]);

            }

            dataTableReader.readHeaders();
            String []headers = dataTableReader.getHeaders();

            String [] output = new String[7];
//Filename        Category_Code   Column_Number   Data_Label      Data_Label_Source       Control_Vocab_Cd        Concept_Type
            output[0] = "Filename";
            output[1] = "Category_Code";
            output[2] = "Column_Number";
            output[3] = "Data_Label";
            output[4] = "Data_Label_Source";
            output[5] = "Control_Vocab_Cd";
            output[6] = "Concept_Type";
            writer.writeNext(output);

            THashSet<String> conceptCount = new THashSet<>();

            log.info("size: " + conceptCount.size());

            dataTableReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
            dataTableReader.readHeaders();
            int fileCounts = dataTableReader.getHeaders().length;
            log.debug("column count: " + fileCounts);
            int outputFiles = fileCounts/_LIMIT +1;
            log.debug("number of output files: " + outputFiles);

            for (int i = 0; i < outputFiles ; i++) {
                File out = new File("C:\\Desktop_Workspace\\sysINFLAME\\KG_EXP2_Microbiome_done\\170126_KINDRED.table.all.OTU_" + i + ".csv");
                log.debug("Writing File: " + out.getName());
                CSVWriter fixedWriter = new CSVWriter(new FileWriter(out));
                fixedWriter.setSeparator('\t');


                dataTableReader = new CSVReader(dataTable.getAbsolutePath(), '\t', "UTF-8");
                int rowCounter = 0;
                int outputSize = i*_LIMIT+_LIMIT < fileCounts ? _LIMIT+1 : fileCounts - (i+1)*_LIMIT+_LIMIT;
                log.debug("outputSize " +outputSize);

                while (dataTableReader.readNext()) {

                    String[] outputString = new String[outputSize];
                    String [] input = dataTableReader.getValues();

                    outputString[0] = rowCounter==0?""+input[0]:""+input[0];//.hashCode();
                    int colCounter = 1;
                    for (int j = i*(_LIMIT)+1; j < (i*_LIMIT)+_LIMIT+1 && j < fileCounts; j++){
                        outputString[colCounter++] = input[j];
                    }
                    fixedWriter.writeNext(outputString);

                    if (rowCounter == 0){
                        //First row, write Config
                        int counter = 1;
                        for (String s : outputString){
                            output[0] = "170126_KINDRED.table.all.OTU_" + i + ".csv";
                            output[1] = "Microbiome";
                            output[2] = ""+counter;
                            output[3] = s.equalsIgnoreCase("new_id")?"SUBJ_ID":sintaxMap.get(s) + "-" + s.replaceAll("_","-");
                            output[4] = "";
                            output[5] = "";
                            output[6] = "NUMERICAL";
                            writer.writeNext(output);
                            counter++;
                        }
                    }
                    rowCounter++;
                }
                fixedWriter.close();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

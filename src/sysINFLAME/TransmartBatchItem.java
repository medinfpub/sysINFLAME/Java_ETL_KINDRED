package sysINFLAME;

/**
 * Created by Baum_Benjamin on 25.04.2017.
 */
public class TransmartBatchItem {

    private String type;
    private String name;

    public TransmartBatchItem(String name, String type){
        this.name = name;


        if (type.equalsIgnoreCase("num")){

            this.type = "NUMERICAL";



        }
        else if (type.equalsIgnoreCase("char")){
            this.type = "CATEGORICAL";
        }

        if (name.contains("gender")){
            this.type = "CATEGORICAL";
        }
        if (name.equalsIgnoreCase("new_id")){
            this.type = "CATEGORICAL";
        }
        if (name.contains("date")){
            this.type = "CATEGORICAL";
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}

package sysINFLAME;

import com.talend.csv.CSVReader;
import com.talend.csv.CSVWriter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Baum_Benjamin on 25.04.2017.
 */
public class KP_EXP11_DIfE {
    static int _ITEMCOUNT = 5;
    static CSVWriter writer;
    static Logger log = Logger.getLogger(KP_EXP11_DIfE.class.getName());
    static CSVReader dataMappingReader;
    public void generateTranSMARTConfig(File folderAbsolutpath,File dataTableAbsolutpath, File mappingFileAbsolutpath){

        try {
            HashMap<String,TransmartBatchItem> columnNameMap = new HashMap<>();
            List<String[]> outputList = new LinkedList<>();

            dataMappingReader = new CSVReader(dataTableAbsolutpath.getAbsolutePath(), ' ', "UTF-8");

            mappingFileAbsolutpath.delete();
            writer = new CSVWriter(new FileWriter(mappingFileAbsolutpath));

            writer.setSeparator('\t');
            while (dataMappingReader.readNext()) {
                String [] input = dataMappingReader.getValues();

                int counter = 0;
                String tmp = "";
                for (String s : input) {

                    // Skip empty coloumns until we have @_ITEMCOUNT items (5)
                    if ((s.equalsIgnoreCase("") || s.isEmpty()) && counter <= _ITEMCOUNT){
                        continue;
                    }

                    // Replace subj_id to new_id (new_id is the hashed columnname by the team in Kiel.
                    s = s.equalsIgnoreCase("subj_id") ? "new_id" : s;


                    // IF we have more than @_ITEMCOUNT items, it is the description of the actual item and not a new column.
                    //Therefore, we just add the words with a space and not a new column
                    tmp += s + (counter >= _ITEMCOUNT ? " " : ";");

                    counter++;

                }
                try {
                    // IF the first item in the output String is a Number, it is a item description, ELSE it's just garbage
                    Integer.parseInt(tmp.split(";")[0]);

                    // Add the item to the list
                    if (!tmp.split(";")[1].equalsIgnoreCase("new_id"))
                        outputList.add(tmp.split(";"));
                    columnNameMap.put(tmp.split(";")[1],new TransmartBatchItem(tmp.split(";")[5],tmp.split(";")[2]));
                    // debug log the item
//                    log.debug(tmp);

                }catch (Exception e){
                    //Do nothing, just skip
                }

            }

            log.debug(columnNameMap);

            // Write transmart-batch config
            String [] output = new String[7];
//Filename        Category_Code   Column_Number   Data_Label      Data_Label_Source       Control_Vocab_Cd        Concept_Type
            output[0] = "Filename";
            output[1] = "Category_Code";
            output[2] = "Column_Number";
            output[3] = "Data_Label";
            output[4] = "Data_Label_Source";
            output[5] = "Control_Vocab_Cd";
            output[6] = "Concept_Type";
            writer.writeNext(output);

            for (File f : folderAbsolutpath.listFiles()) {
                if (f.getName().endsWith(".csv") && !f.getName().startsWith("clinical_mapping")) {
                    String currentFileName = f.getName();
                    log.debug(f);


                    CSVReader dataFileReader = new CSVReader(f.getAbsolutePath(), '\t', "UTF-8");
                    dataFileReader.readHeaders();
                    int counter = 1;
                    for (String header : dataFileReader.getHeaders()) {


                        output[0] = currentFileName;
                        output[1] = "DIfE Data + " + currentFileName.substring(0, currentFileName.indexOf("."));
                        output[2] = "" + counter;
                        output[3] = header.equalsIgnoreCase("new_id") ? "SUBJ_ID" : columnNameMap.containsKey(header) ? columnNameMap.get(header).getName() + "_" + header : header;
                        output[4] = "";
                        output[5] = "";
                        output[6] = columnNameMap.containsKey(header) ? columnNameMap.get(header).getType() : "CATEGORICAL";
                        writer.writeNext(output);

                        counter++;
                    }
                }
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

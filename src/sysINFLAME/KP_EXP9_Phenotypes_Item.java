package sysINFLAME;

import java.util.HashMap;

/**
 * Created by Baum_Benjamin on 26.04.2017.
 */
public class KP_EXP9_Phenotypes_Item {


    public void add(String value, String value_description){
        switch (value_description) {
            case "continuous":
                break;
            case "unreadable/inconsistent":
                break;
            case "missing":
                break;
            default:
                valueMap.put(value,value_description);

        }

    }

    public HashMap<String, String> getValueMap() {
        return valueMap;
    }

    public void setValueMap(HashMap<String, String> valueMap) {
        this.valueMap = valueMap;
    }

    HashMap<String,String> valueMap = new HashMap<>();

    private String trait;
    private String trait_id;
    private String trait_name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    public String getTrait() {
        return trait;
    }

    public void setTrait(String trait) {
        this.trait = trait;
    }

    public String getTrait_id() {
        return trait_id;
    }

    public void setTrait_id(String trait_id) {
        this.trait_id = trait_id;
    }

    public String getTrait_name() {
        return trait_name;
    }

    public void setTrait_name(String trait_name) {
        this.trait_name = trait_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue_description() {
        return value_description;
    }

    public void setValue_description(String value_description) {
        this.value_description = value_description;
    }

    private String description;
    private String value;
    private String value_description;

    public String getSource_token() {
        return source_token;
    }

    public void setSource_token(String source_token) {
        this.source_token = source_token;
    }

    private String source_token;

    public KP_EXP9_Phenotypes_Item(String trait, String trait_id, String trait_name, String description, String value, String value_description, String source_token) {
        this.trait = trait;
        this.trait_id = trait_id;
        this.trait_name = trait_name;
        this.description = description;
        this.value = value;
this.source_token = source_token;
        this.value_description = value_description;

        try{
            int v = Integer.parseInt(value);
            valueMap.put(value,value_description);
        }catch (Exception e){

        }

    }

    @Override
    public String toString() {
        return this.trait + " " + this.getType()+ " "  + this.description + " " + this.getValueMap();
    }
}

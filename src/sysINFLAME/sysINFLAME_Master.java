package sysINFLAME;

import java.io.File;

/**
 * Created by Baum_Benjamin on 01.06.2017.
 */
public class sysINFLAME_Master {

    private static void KG_EXP2_Microbiome(){
        File dataTable = new File("170126_KINDRED.table.all.OTU.csv");
        File sintax = new File("170126_KINDRED.OTU.rdp.tax");
        KG_EXP2_Microbiome kg_exp2_microbiome = new KG_EXP2_Microbiome();
        kg_exp2_microbiome.generateTranSMARTConfig(dataTable,sintax);
    }

    private static void KP_EXP11_DIfE(){
        /**
         * dataTableAbsolutpath and mappingFileAbsolutpath must be in folderAbsolutpath. folderAbsolutpath is still
         * necessary.
         */
        File folderAbsolutpath = new File("KP_EXP11_DIfE_done");
        File dataTableAbsolutpath = new File("datadescription_ffq2_with_PA.txt");
        File mappingFileAbsolutpath = new File("clinical_mapping_KP_EXP11.csv");
        KP_EXP11_DIfE kp_exp11_dIfE = new KP_EXP11_DIfE();
        kp_exp11_dIfE.generateTranSMARTConfig(folderAbsolutpath,dataTableAbsolutpath,mappingFileAbsolutpath);
    }
    private static void KP_EXP9_Phenotypes(){
        File dataTable = new File("phenotypes.txt");
        File dataTableMapped = new File("phenotypes_MAPPED.csv");
        File codebook = new File("codebook.xlsx");
        File mappingFile = new File("clinical_mapping_KP_EXP9.csv");
        KP_EXP9_Phenotypes kp_exp9_phenotypes = new KP_EXP9_Phenotypes();
        kp_exp9_phenotypes.generateTranSMARTConfig(dataTable,dataTableMapped,codebook,mappingFile);
    }

    public static void main(String[] args){

        /**
         * KP_EXP9_Phenotypes
         */
//        KP_EXP9_Phenotypes();


        /**
         * KP_EXP11_DIfE
         */
        KP_EXP11_DIfE();


        /**
         * KG_EXP2_Microbiome
         */

//        KG_EXP2_Microbiome();

    }
}
